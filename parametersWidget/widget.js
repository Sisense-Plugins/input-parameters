
prism.registerWidget('parameters', {
	name: 'parameters',
	family: 'parameters',
	title: "Parameters",
	iconSmall: "/plugins/parametersWidget/icon.png",
	styleEditorTemplate: "/plugins/parametersWidget/styler.html",
	directive: {
		desktop: 'parameters'
	},
	style: {
		autoUpdate: true
	},
	data:  {
		selection: ['items'],
		defaultQueryResult: {},
		panels: [
			{
				name: 'items',
				type: 'visible',
				metadata: {
					types: ['dimensions'],
					maxitems: -1,
					sortable: {
						maxitems: 1
					}
				}
			},
			{
				name: 'filters',
				type: 'filters',
				metadata: {
					types: ['dimensions'],
					maxitems: -1
				}
			}
		],

		allocatePanel: function (widget, metadataItem) {

			return "items";
		},

		// returns true/ reason why the given item configuration is not/supported by the widget
		isSupported: function (items) {

			var a = prism.$jaql.analyze(items);

			return a.dimensions.length > 0 && a.measures.length === 0;
		},

		// ranks the compatibility of the given metadata items with the widget
		rankMetadata: function (items, type, subtype) {

			var a = prism.$jaql.analyze(items);

			// require at least 1 dimension
			if (a.measures.length > 0 || a.dimensions.length === 0) {

				return -1;
			}

			return 0;
		},

		// populates the metadata items to the widget
		populateMetadata: function (widget, items) {

			var a = prism.$jaql.analyze(items);

			// allocating dimensions
			widget.metadata.panel(0).push(a.dimensions);

			// allocating filters
			widget.metadata.panel("filters").push(a.filters);
		},

		// builds a jaql query from the given widget
		buildQuery: function (widget) {
	    
			//
			// building jaql query object from widget metadata 
			var query = { datasource: widget.datasource, metadata: [] };

			widget.metadata.panel(0).items.forEach(function (item) {

				query.metadata.push($$.object.clone(item, true));
			});

			// series - dimensions
			widget.metadata.panel('filters').items.forEach(function (item) {

				item = $$.object.clone(item, true);
				item.panel = "scope";

				query.metadata.push(item);
			});

			return query;
		},

		// prepares the widget-specific query result from the given result data-table
		processResult: function (widget, queryResult) {
	   
			return queryResult;
		}
	},
	mask: {
		numeric: function (widget, item) {
			return {
				type: "number",
				abbreviations: {
					t: true,
					b: true,
					m: true,
					k: false
				},
				separated: true,
				decimals: "auto"
			};
		}
	},
	options: {
		dashboardFiltersMode: "highlight",
		selector: false
	},
	sizing: {
		minHeight: 32, //header
		maxHeight: 2048,
		minWidth: 128,
		maxWidth: 2048,
		height: 64,
		defaultWidth: 256
	}
});
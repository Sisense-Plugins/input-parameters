// Added by Moti Granovsky, Dec 2016
// Polyfill for String.prototype.endsWith function which is an ES6 function and does not exist in IE 10/11
if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}

//
// parameters plugin.
//

prism.run(["$jaql", function ($jaql)
{
	// 
	// Query Handling
	// --------------

	// drill through the given object and look for available parameters in the context matching any of the object's placeholders
	function drill(o, context) {

		var v, t, f, m, a;		
		for (var p in o) {

			if (!o.hasOwnProperty(p)) {

				continue;
			}

			v = o[p];

			// looking for placeholder
			if (defined(t = $$get(v, "title")) && typeof t === 'string' && t[0] === '@') {

				t = t.substring(1);
				a = m = context[t];
				
				if (!defined(m) && t.endsWith(".value")) {					
					t = t.substring(0, t.indexOf(".value"));
					a = context[t];
					//m = parseFloat($$get(a, "filter.equals"));					
					m = prism.parameters.dictionary[t]
				}

				// custom value provider
				if (defined(context.$custom)) {

					m = null;
					a = context.$custom(t) || a;
				}

				
				if (defined(m)) {
					
					// applying value
					o[p] = m;
					
					if (defined(a, "$$guid")) {
						context.$metadata.remove(context.$filtersmap[a.$$guid]);
					}
				}
			}

			// continue drilling
			if (Array.isArray(v) || typeof (v) === "object") {
				
				drill(v, context);
			}
		}
	}

	//
	// generate name -> value context map that will be used to apply parameter values to query placeholders. 
	// Also, a custom value provider can be used when attached to the widget.options object to allow resolving placeholders values in ways exstinging the default widget/dashboard filters.
	//
	// I/E -
	//
	// dashboard.$customParameterValueProvider = function(name) {
	//		
	//		if (name === "A") return { ... };
	// }

	function buildContext(context, arr) {

		var f, title;
		for (var i = 0 ; i < arr.length ; i++) {

			f = arr[i];
			title = $$get(f, "jaql.title");

			if (defined(title)) {

				context[title] = f.jaql;

				// mapping source -> query item to remove filters that were used as parameter value
				var mitem = context.$metadata.filter(function (m) { 
					return m.panel === "scope" && !$jaql.isMeasure(m) && $$get(m, "jaql.dim") === $$get(f, "jaql.dim") && $$get(m, "jaql.level") === $$get(f, "jaql.level") 
				});
				if (mitem.length === 1) {
					 
					f.jaql.$$guid = $$guid(8);		
					context.$filtersmap[f.jaql.$$guid] = mitem[0];
				}
					
			}
		}
	}

	function getContext(widget, query) {

		//	Create a context object
		var context = {};
		context.$filtersmap = {};
		context.$metadata = query.metadata;		

		buildContext(context, widget.dashboard.filters.$$items);
		buildContext(context, widget.metadata.filters());

		// setting custom provider
		context.$custom = $$get(widget.dashboard, "$customParameterValueProvider");		
		return context;
	}

	// gets the widget before query event and tryng to apply parameter values to existing query placeholders
	function processWidgetQuery(s, e) {

		var widget = e.widget;
		var query = e.query;

		// generating context
		var context = getContext(widget, query);
		
		// applying parameter values
		drill(query.metadata, context);		

		// cleaning $$guid attributes from context items
		for (var property in context) {

			if (!context.hasOwnProperty(property)) {

				continue;
			}

			if (defined(context, property+".$$guid")) {

				delete context[property].$$guid;
			}
		}
	}

	// 
	// Hooking-up
	// ----------

	var handler;

	// uses to intercept all widget queries of a given dashboard and apply dynamic values into predefined placeholders 
	function dashboardHandler(dashboard) {

		// release all references held by the dashboard handler
		this.destory = function () {

			dashboard.off("widgetbeforequery", processWidgetQuery);
		};

		// hooking up to the dashboard
		dashboard.on("widgetbeforequery", processWidgetQuery);
	}

	//	Load the default parameter values
	function buildParameters(){

		//	Get the Elasicube name
		var elasticubeName = prism.activeDashboard.datasource.title;

		//	Get the table name
		var tableName = prism.parameters.settings.table;

		//	Array to save the parameters
		var parameterFields = [];

		//	Get the fields dynamically
		var getFields = function(data) {
			
			//	Create a payload object
			var payload = {
				datasource: prism.activeDashboard.datasource,
				metadata: []
			}

			//	Save each object to the dictionary
			$.each(data,function(){

				//	Make sure this belongs to the parameters table
				if (this.table.toLowerCase() == tableName.toLowerCase()) {

					//	Create a jaql object for this dim
					var thisJaql = {
						jaql: {
							dim: this.id
						}
					};

					//	Add to payload
					payload.metadata.push(thisJaql);
				}
			});

			//	Create the API url for fetching data
			var apiUrl = '/jaql/query';
			
			//	Run the API call to fetch data for these values
			if (data.length==0){
				console.log("Parameters Plugin: Whoops! Could not find the " + tableName + " table...");
				return;
			} else {
				//	Run another AJAX call to get the default values
				$.ajax({
					method: 'POST',
					url: apiUrl,
					async: false,
					data: JSON.stringify(payload),
					contentType: "application/json"
				}).done(createDictionary);
			}
		};

		//	Create the dictionary from the default values
		var createDictionary = function(data) {
			
			//	Get the dictionary
			var parameters = prism.parameters.dictionary;

			//	Loop through each parameter
			for (var i=0; i<data.headers.length; i++){

				//	Save the parameters to the dictionary
				parameters[data.headers[i]] = data.values[0][i].data;
			}			
		}

		//	Create the API url
		var apiUrl = '/api/elasticubes/metadata/' + elasticubeName + '/fields?q=' + tableName;

		//	Run the API call
		$.ajax({
			method: 'GET',
			url: apiUrl,
			async: false
		}).done(getFields);
	}

	// hooking up to all dashboard queries on load
	prism.on("dashboardloaded", function (e, args) {

		handler = new dashboardHandler(args.dashboard);

		//	Build the parameters and get the default values
		buildParameters();
	});

	// removing hooks from dashboard on unload
	prism.on("dashboardunloaded", function () {
		
		if (!defined(handler)) {

			return;
		}

		handler.destory();
		handler = null;
	});
}]);



mod.directive('parameters', [
    '$timeout',
    'ux-controls.services.$dom',
    function ($timeout, $dom) {
    	return {
    		priority: 0,
    		replace: false,
    		templateUrl: "/plugins/parametersWidget/widget-template.html",
    		transclude: false,
    		restrict: 'E',
    		link: function link($scope, lmnt, attrs) {

                //  Define the default value
                var defaultParameterValue = 1;

                //  Add the style properties to the scope
                $scope.autoUpdate = $scope.widget.style.autoUpdate;                

    			//	Get all the parameters specified
    			var dimensions = $scope.widget.metadata.panels[0].items;
                var dictionary = prism.parameters.dictionary;

    			//	Loop through them and add them to the array
                $scope.parameters = [];
                $.each(dimensions, function(){

                    //  Look for a default value
                    var defaultValue = (dictionary && dictionary[this.jaql.column]) ? dictionary[this.jaql.column] : defaultParameterValue;
                    
                	//	Create an object to save the properties of this panel item                    
                	var item = {
                		dim: this.jaql.dim,
                		title: this.jaql.title,
                        column: this.jaql.column,
                		datatype: this.jaql.datatype,
                        defaultValue : defaultValue
                	};

                	//	Add to the list 
                	$scope.parameters.push(item);
                })

                //  Function to reset the filter inputs
                $scope.reset = function(){

                    //  Loop through the parameters and save to the prism object
                    $.each(this.parameters, function(){   

                        //  Reset each value in the dictionary
                        this.value = this.defaultValue;                        
                    })

                    //  Save the html Get the input boxes
                    var parameterElements = $('input[ng-model="parameter.value"]',$('widget[widgetid="'+$scope.widget.oid+'"]'));

                    //  Reset the selections
                    parameterElements.val(null);

                    //  Update the dashboard
                    this.setFilter(this);
                }

                //  Function that runs when an input was updated
                $scope.inputSet = function(event){                    

                    //  Only do something when auto-update set to true
                    if ($scope.autoUpdate) {

                        //  Check the event type
                        if (event.type == "blur") {
                            //  focus was moved to another element, update
                            $scope.setFilter($scope);
                        } else if (event.type == "keyup" && event.keyCode == 13) {
                            //  use hit enter, update
                            $scope.setFilter($scope);
                        }
                    }
                }

                //  Function to set the dashboard filter
				$scope.setFilter = function(formScope){ 
                    

                    //  Loop through the parameters and save to the prism object
                    $.each(formScope.parameters, function(){   

                        //  Get the value for this parameter
                        var thisValue = (typeof this.value == "number") ? this.value : this.defaultValue;

                        //  Set in the dictionary
                        prism.parameters.dictionary[this.column] = thisValue;
                    })

                    //  Refresh the dashboard
                    prism.activeDashboard.refresh();
				}

                //  Function to calculate the label widths after load
                $timeout(function(){
                    
                    //  Get the container element
                    var widget = $('widget[widgetid=' + $scope.widget.oid + ']');

                    //  Get the parameters container
                    var container = $('div.parametersContainer',widget);
                    
                    //  Get the labels
                    var labels = $('label',container);

                    //  Calculate the largest label
                    var maxWidth = 0;
                    $.each(labels,function(){
                        var labelWidth = $(this).width();
                        if (labelWidth>maxWidth){
                            maxWidth = labelWidth;
                        }
                    })
                    
                    //  Adjust all labels to this width
                    labels.width(maxWidth);
                },0)
    		}
    	}
    }]);

